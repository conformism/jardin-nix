{ terraformService, records, zoneId }:
let
  adapter = import ./adapter.nix;
  terraformSources = {
    inherit records; inherit zoneId;
  };
  terraform = terraformService terraformSources;
in
{
  # TODO: write run config
  run = ();
}
