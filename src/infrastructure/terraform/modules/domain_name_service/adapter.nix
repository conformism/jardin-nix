{ records, zoneId }:
{
  resource.cloudflare_record = builtins.listToAttrs (
    map
      (record: {
        name = record.hostname;
        value = {
          zone_id = zoneId;
          name = record.name;
          value = record.value;
          type = records.type;
          ttl = records.ttl;
        };
      })
      records
  );
}
