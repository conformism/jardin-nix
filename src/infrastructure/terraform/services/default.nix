{terraformSources}:
{
  run = (service:
    service terraformSources
  )
  terranix = import ./terranix.nix;
}
