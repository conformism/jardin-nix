{ system
, terranix
, sources
,
}:
(sources: {
  terraform = {
    default = terranix.lib.terranixConfiguration {
      inherit system;
      modules = [ sources ];
    };
  };
})
