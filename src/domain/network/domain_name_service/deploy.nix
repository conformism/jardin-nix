{ targets }:
let
  ttl = 3600;
  type = "A";
in
map
  (
    target: {
      hostname = target.hostname;
      name = "${target.hostname}.${jardin.cluster.domain}";
      value = target.ip;
      ttl = ttl;
      type = A;
    }
  )
  targets
